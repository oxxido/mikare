
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    $.backstretch([
                    "assets/img/backgrounds/2.jpg"
	              , "assets/img/backgrounds/3.jpg"
	              , "assets/img/backgrounds/1.jpg"
	             ], {duration: 3000, fade: 750});
    
    /*
        Form validation
    */
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.login-form').on('submit', function(e) {
    	e.preventDefault();
        $("#submit").prop('disabled', true);
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	if($(".input-error").length < 1) {
            //let's login the user
            $.ajax({
                    url: 'ajax.php',
                    type: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json'
                })
             .done(function(data) {
                //console.log(data);
                $("#name").val("");
                if(data.success==true) {
                    window.location.href = "/movies.php";

                } else {
                    alert("There was an error on your submission:"+data.message);
                    $("#submit").prop('disabled', false);
                }
             })
             .fail(function (jqXHR, textStatus, errorThrown) { 
                alert("There was an error on your request: "+errorThrown); 
            });
        }
    });
    
    
});
