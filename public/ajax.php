<?php
//setting header response
header('Content-Type: application/json');

//require main init file
require_once("init.php");

//starting empty response object
$response = new stdClass;
$response->success = false;
$response->message = "No action defined";

//switch actions
switch ($_POST['action']) {
    case 'login':
        if (trim($_POST['username'])=="" || trim($_POST['password'])=="") {
            $response->message = 'Please type both user and password';
            break;
        }
        $sth = $dbh->prepare('SELECT id, password FROM users WHERE
            username = :username LIMIT 1');
        $sth->bindParam(':username', $_POST['username']);
        $sth->execute();

        $user = $sth->fetch(PDO::FETCH_OBJ);

        $checked = $user && password_verify($_POST['password'], $user->password);
        if ($checked) {
            //save user session
            $_SESSION["logged"] = true;
            $_SESSION["username"] = $_POST['username'];
            $_SESSION["id"] = $user->id;
            $_SESSION["token"] = getToken($_POST['username'],$user->id);
            $response->success = true;
            $response->message = 'Access granted';
            $response->token = $_SESSION["token"];
        } else {
            $response->message = "Your user/password don't match.";
        }
        //preparing message to save log
        $log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
        "Attempt: ".($checked? 'Success' : 'Failed').PHP_EOL.
        "User: ".$_POST['username'].PHP_EOL.
        "Pass: ".$_POST['password'].PHP_EOL.PHP_EOL;
        write_log($log);
        
        break;
    case 'add_movie':
        if (trim($_POST['name'])=="" || trim($_POST['year'])=="") {
            $response->message = 'Please type both movie and release year';
            break;
        }
        if (intval($_POST['year']) > date("Y") OR intval($_POST['year']) < 1900) {
            $response->message = 'Release year is out of range: '.$_POST['year'];
            break;
        }
        if(!isset($user_id)) {
            $response->message = 'You need to be logged to perform this action';
            break;
        }
        try {
            // add movie
            $stmt = $dbh->prepare("INSERT INTO movies(`user_id`, `name`, `release`) 
                VALUES (:user_id, :name, :release)");
            $values = array(':user_id' => $user_id,
                ':name' => $_POST['name'], 
                ':release' => $_POST['year']);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':name', $_POST['name']);
            $stmt->bindParam(':release', $_POST['year']);
            $stmt->execute();

            if($stmt->rowCount() > 0){
                $response->success = true;
                $response->message = "Movie succesfully added";
            } else {
                $response->message = "There was a problem adding movie, please try again."+$dbh->errorCode();
                //$response->message .= print_r($values, true);
                $response->error = array();
                foreach($dbh->errorInfo() as $error)
                {
                $response->error[] = $error;
                }
            }

            }
        catch(PDOException $e)
            {
            $response->message = $e->getMessage();
            }
        break;
    case 'get_movies':
        break;
}

echo json_encode($response);

?>