<?php
//require main init file
$private = 'yes';
require_once("init.php");
$search_result = "";
//check if search is set
if(isset($_POST['search']) && trim($_POST['search'])!="") {
    //get current user movies
    $sth = $dbh->prepare('SELECT * FROM movies WHERE `user_id` = :user_id AND `name` LIKE :search');
    $search = "%".trim($_POST['search'])."%";
    $sth->bindParam(':search', $search);
    $search_result = "Searching for ".trim($_POST['search']);
} else {
    //get current user movies
    $sth = $dbh->prepare('SELECT * FROM movies WHERE `user_id` = :user_id');
}

$sth->bindParam(':user_id', $user_id);
$sth->execute();

$movies = $sth->fetchAll(PDO::FETCH_CLASS);
$years = range(date("Y"), 1900);
//separating "controller" from "view"
require_once("movies.html");
?>