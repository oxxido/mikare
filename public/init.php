<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
require_once("functions.php");

//check if logging out first
if(isset($_GET['logout'])) {
    //saving log
    $log  = "IP: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
        "Action: Logging out.".PHP_EOL.
        "User: ".$_SESSION["username"].PHP_EOL.PHP_EOL;
    write_log($log);
    //logging out and redirecting to login
    unset($_SESSION["logged"]);
    unset($_SESSION["username"]);
    unset($_SESSION["id"]);
    unset($_SESSION["token"]);
    //redirect and exit:
    header('Location: /login.php');
    exit();
}

//after initialize session, load config and then start variables
$config = parse_ini_file("../config.ini");

$cost = "10";
$b = "<br />\n\r";


//check if page is private, if so, redirect to login if no user
if(isset($private) && $private="true") {
    if(!isset($_SESSION["logged"]) OR $_SESSION["logged"]!=true) {
        $_SESSION['flash_message'] = "You are not authorized to see this page";
        header('Location: login.php');
        exit();
    }
}

if(isset($_SESSION["logged"]) && $_SESSION["logged"]==true) {
    $username = $_SESSION["username"];
    $user_id = $_SESSION["id"];
    $token = $_SESSION["token"];
}
//init db
$dbh = new PDO('mysql:host=localhost;dbname='.$config['dbName'], 
    $config['dbUser'], 
    $config['dbPassword']);