<?php
function getToken($username, $id)
{
    //add vars and get token
    $mixer = $username.".-.".$id;
    return password_hash($mixer, PASSWORD_DEFAULT, ['cost' => 10]);
}
function checkToken($token, $username, $id)
{
    if(trim($token)=="" || trim($username)=="" || trim($id)=="") {
        return false;
    }
    $mixer = $username.".-.".$id;
    return password_verify($mixer, $token);
}
function write_log($message) {
    //save info to log
    return file_put_contents('sessions.log', $message, FILE_APPEND);

}